set -e

mkdir -p "./.cache/fr/cities/"

echo "var geoDataCitiesFr = {\"type\":\"FeatureCollection\",\"features\":[" > ./geo-data-cities-fr.js

for file in ./data/fr/cities/*.yml; do
	[ -f "$file" ] || continue
	echo "Parsing $file..."
	code=$(basename "$file" .yml)
	description=$(cat $file | yq -r .fr.description)
	description=$(sed 's/"/\\"/g' <<<$description)
	url=$(cat $file | yq -r .fr.url)
	if [ ! -f "./.cache/fr/cities/${code}.txt" ]; then
		cache=$(curl --silent "https://geo.api.gouv.fr/communes?code=${code}&fields=name,centre,contour")
		echo "${cache}" > "./.cache/fr/cities/${code}.txt"
	else
		cache=$(cat "./.cache/fr/cities/${code}.txt")
	fi
	nom=$(echo ${cache} | jq -c -r .[0].nom)
	lng=$(echo ${cache} | jq .[0].centre.coordinates[0])
	lat=$(echo ${cache} | jq .[0].centre.coordinates[1])
	geo=$(echo ${cache} | jq -c .[0].contour)
	if [ "$url" == "null" ]; then
		echo "{\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\",\"marker\":[$lat,$lng]},\"geometry\":$geo}," >> ./geo-data-cities-fr.js
	else
		echo "{\"type\":\"Feature\",\"properties\":{\"name\":\"$nom\",\"description\":\"$description\",\"marker\":[$lat,$lng],\"url\":\"$url\"},\"geometry\":$geo}," >> ./geo-data-cities-fr.js
	fi
done

echo "]};" >> ./geo-data-cities-fr.js