#!/usr/bin/env python3

import os
import signal
import subprocess
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from PIL import Image

from pyvirtualdisplay import Display

display = Display(visible=False, size=(1024, 768))
display.start()

cmd = 'python -m http.server --bind 127.0.0.1 9999'
process = subprocess.Popen(cmd, shell=True, preexec_fn=os.setsid)
time.sleep(1)

chrome_options = Options()
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--headless")
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument("--disable-extensions")
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--disable-dev-shm-usage")
chrome_options.add_argument("--enable-automation")
chrome_options.add_argument("--window-size=1024,768")
chrome_options.add_argument("--user-data-dir=/tmp/chromium-tmp-dir")

browser = webdriver.Chrome(options=chrome_options, desired_capabilities={})
browser.get('http://localhost:9999/')
time.sleep(5)
browser.save_screenshot('screenshot.png')
os.killpg(process.pid, signal.SIGKILL)
browser.quit()
display.stop()

png = Image.open("screenshot.png")
jpg = png.convert("RGB")
jpg.save('screenshot.jpg', quality=90, optimize=True, progressive=True)
os.remove("screenshot.png")
